﻿using UnityEngine;
using System.Collections;

namespace MyLib
{
    public class GeneraPhysic : MonoBehaviour
    {
        public virtual void TurnTo(Vector3 dir) {
        }

        public virtual void MoveSpeed(Vector3 mv) {
        }

        public virtual void TurnTower(Vector3 dir) {
        }

    }
}
